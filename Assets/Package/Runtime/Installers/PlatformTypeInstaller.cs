using Attaboys.PlatformType.Package.Runtime.Db.Impls;
using AttaboysGames.Package.Runtime;
using UnityEngine;
using Zenject;

namespace Attaboys.PlatformType.Package.Runtime.Installers
{
	[CreateAssetMenu(menuName = "Installers/Attaboys/" + nameof(PlatformTypeInstaller),
		fileName = nameof(PlatformTypeInstaller))]
	public class PlatformTypeInstaller : ScriptableObjectInstaller
	{
		[SerializeField] private PlatformTypeDatabase platformTypeDatabase;

		public override void InstallBindings()
		{
			Container.BindDatabase(platformTypeDatabase);
		}
	}
}