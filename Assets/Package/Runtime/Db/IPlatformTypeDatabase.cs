namespace Attaboys.PlatformType.Package.Runtime.Db
{
	public interface IPlatformTypeDatabase
	{
		EPlatformType GetPlatform();
	}
}