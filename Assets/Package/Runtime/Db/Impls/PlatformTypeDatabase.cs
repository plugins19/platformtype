using UnityEngine;

namespace Attaboys.PlatformType.Package.Runtime.Db.Impls
{
	[CreateAssetMenu(menuName = "Databases/Attaboys/" + nameof(PlatformTypeDatabase),
		fileName = nameof(PlatformTypeDatabase))]
	public class PlatformTypeDatabase : ScriptableObject, IPlatformTypeDatabase
	{
		public EPlatformType GetPlatform()
		{
#if UNITY_EDITOR
			return EPlatformType.Editor;
#endif
			
#if UNITY_IOS
			return EPlatformType.IOS;
#endif
			
#if UNITY_ANDROID
			return EPlatformType.Android;
#endif
			
#if UNITY_WEBGL
			return EPlatformType.WebGl;
#endif
		}
	}
}