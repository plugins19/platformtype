namespace Attaboys.PlatformType.Package.Runtime.Db
{
	public enum EPlatformType : byte
	{
		Editor,
		Android,
		IOS,
		WebGl
	}
}